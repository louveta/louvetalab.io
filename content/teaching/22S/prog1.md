+++
title = "Prog 1"
weight = 2200000
[extra]
semester="Spring 2022"
professor = "Pierre Rousselin"
university="USPN (Institut Galilée)"
prof_website = "https://www.math.univ-paris13.fr/~rousselin/"
level = "L0"
+++
