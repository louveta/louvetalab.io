+++
title = "Init Web"
weight = 2200002
[extra]
professor = "Carlos Olarte"
prof_website = "https://sites.google.com/site/carlosolarte/"
semester="Spring 2022"
university="USPN (Institut Galilée)"
level = "L1"
+++

Github repo with the class content on [Carlos Olarte's github](https://github.com/carlosolarte/init-web)
