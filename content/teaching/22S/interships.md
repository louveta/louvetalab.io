+++
title = "Interships supervision"
weight = 2200001
[extra]
professor = "Nadi Tomeh"
semester="Spring 2022"
university="USPN (Institut Galilée)"
prof_website = "https://lipn.univ-paris13.fr/~tomeh/"
level = "L3"
+++
