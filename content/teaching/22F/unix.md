+++
title = "Unix"
weight = 2210001
[extra]
professor = "Pierre Rousselin"
semester="Fall 2022"
university="USPN (Institut Galilée)"
prof_website = "https://www.math.univ-paris13.fr/~rousselin/"
level = "L1"
+++
