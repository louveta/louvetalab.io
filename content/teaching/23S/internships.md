+++
title = "Interships supervision"
weight = 2300000
[extra]
professor = "Nadi Tomeh"
semester="Spring 2023"
university="USPN (Institut Galilée)"
prof_website = "https://lipn.univ-paris13.fr/~tomeh/"
level = "L3"
+++
