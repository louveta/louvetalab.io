+++
title = "Interships supervision"
weight = 2400002
[extra]
professor = "Nadi Tomeh"
semester="Spring 2024"
university="USPN (Institut Galilée)"
prof_website = "https://lipn.univ-paris13.fr/~tomeh/"
level = "L3"
+++
