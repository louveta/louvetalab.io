+++
title = "Prog 1"
weight = 2400001
[extra]
semester="Spring 2024"
professor = "Sylvie Borne"
university="USPN (Institut Galilée)"
prof_website = "https://lipn.univ-paris13.fr/~borne/"
level = "L0"
+++
