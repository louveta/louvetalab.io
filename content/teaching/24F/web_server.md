+++
title = "Programmation Web"
weight = 2410001
[extra]
professor = "Jean-Christophe Dubacq"
prof_website = "https://lipn.fr/~dubacq/"
semester="Fall 2024"
university="USPN (IUT Villetaneuse)"
level = "BUT2"
+++

Document available on [Jean-Christophe Dubacq's website](https://lipn.fr/~dubacq/r301/)
