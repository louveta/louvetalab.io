+++
title = "Programmation Systeme"
weight = 2410002
[extra]
professor = "Davide Buscali"
prof_website = "https://sites.google.com/site/davidebuscaldi/"
semester="Fall 2024"
university="USPN (IUT Villetaneuse)"
level = "BUT2"
+++

Document available on [Davide Buscali's website](https://sites.google.com/site/davidebuscaldi/teaching/systeme)

[Revision sheet + answers](/media/classes/prog_sys_rep.pdf)

[Revision sheet](/media/classes/prog_sys_norep.pdf)
