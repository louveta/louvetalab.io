+++
title = "Qualite Algo"
weight = 2410000
[extra]
professor = "Axel Bacher"
prof_website = "https://lipn.fr/~bacher/"
semester="Fall 2024"
university="USPN (IUT Villetaneuse)"
level = "BUT3"
+++

Document available on [Axel Bacher's website](https://lipn.fr/~bacher/algo/cours.pdf)
