+++
title = "A little bit of academic genealogy"
date = 2022-07-06
+++

I got aware of the existence of the [Maths Genealogy Project](https://genealogy.math.ndsu.nodak.edu/) thanks to a friend. It aims at connecting mathematicians (and therefore computer scientists considering the root of the field) together through academic ancestry: A PhD supervisor of someone being the equivalent of a "parent".

There are quite a few projects on github to scrap and vizualize the data. I used [mgptree by Joe Thomas](https://github.com/jsthomas/mgptree) to make this pretty cool picture of my own genealogy tree.

[<img src="../../media/tree.png" title="my academic genealogy tree" class="article_picture">](../../media/tree.png)
