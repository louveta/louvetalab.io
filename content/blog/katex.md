+++
title = "Adding KaTeX support to zola"
date = 2023-06-14
+++

By using the [even theme](https://github.com/getzola/even) as an example, I was able to add \\(\KaTeX\\) support to this blog which is very convenient for article abstracts (part of this explanation is also from their github repository README). To do the same:

- Add to the header of your main html template (`base.html` for me) which will import the [katex](https://katex.org/) js and css:
```html
        {% block js %}
        {% if config.extra.katex_enable %}
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0/dist/katex.min.css" 
        integrity="sha384-9eLZqc9ds8eNjO3TmqPeYcDj8n+Qfa4nuSiGYa6DjLNcv9BtN69ZIulL9+8CqC9Y" crossorigin="anonymous">

        <script defer src="https://cdn.jsdelivr.net/npm/katex@0.10.0/dist/katex.min.js" 
        integrity="sha384-K3vbOmF2BtaVai+Qk37uypf7VrgBubhQreNQe9aGsz9lB63dIFiQVlJbr92dw2Lx" crossorigin="anonymous"></script>
        <script defer src="https://cdn.jsdelivr.net/npm/katex@0.10.0/dist/contrib/mathtex-script-type.min.js" 
        integrity="sha384-zWYbd0NBwgTsgIdFKVprSfTh1mbMPe5Hz1X3yY4Sd1h/K1cQoUe36OGwAGz/PcDy" crossorigin="anonymous"></script>
            {% if config.extra.katex_auto_render %}
        <script defer src="https://cdn.jsdelivr.net/npm/katex@0.10.0/dist/contrib/auto-render.min.js" 
        integrity="sha384-kmZOZB5ObwgQnS/DuDg6TScgOiWWBiVt0plIRkZCmE6rDZGrEOQeHM5PcHi+nyqe" crossorigin="anonymous"
                onload="renderMathInElement(document.body);"></script>
            {% endif %}
        {% endif %}
        {% endblock js %}

        {% block css %}
            {% if config.extra.katex_enable %}
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0/dist/katex.min.css" 
            integrity="sha384-9eLZqc9ds8eNjO3TmqPeYcDj8n+Qfa4nuSiGYa6DjLNcv9BtN69ZIulL9+8CqC9Y" crossorigin="anonymous">
            {% endif %}
        {% endblock css %}
```

- Create the file `templates/shortcodes/katex.html` with content:
```html
        <script type="math/tex{% if block %};mode=display{% endif %}">{{body | safe}}</script>
```

- Adding this bit to your `config.toml`, enables the use of ```{ { katex(body="\KaTeX") } }``` and ```{ % katex(block=true) %}\KaTeX{% end % }```
```toml
        [extra]
        katex_enable = true
```
- Or Adding this bit to your `config.toml`, enables `\\( \KaTeX \\)`, `\\[ \KaTeX \\]` and `$$ \KaTeX $$`
```toml
        [extra]
        katex_enable = true
        katex_auto_render = true
```
