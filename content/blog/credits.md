+++
title = "Information for this website"
date = 2022-07-02
+++

This website was made using [zola](https://www.getzola.org/), that's a nice rust-written framework to generate static websites.

You can get the source code of the website on [gitlab](https://gitlab.com/louveta/louveta.gitlab.io).

The picture of me was taken during the [Tohoku TedX conference on AI](https://www.tohoku.ac.jp/en/news/university_news/news20190128_tedx.html) in January 2019.
