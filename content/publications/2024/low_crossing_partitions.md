+++
title = "A Greedy Algorithm for Low-Crossing Partitions for General Set Systems"
date = 2024-07-18
[extra]
links = [["ALENEX25 proceedings","https://epubs.siam.org/doi/10.1137/1.9781611978339.17"],["Full version (HAL)","https://hal.science/hal-04881799"]]
authors = ["Monika Csikos,", "Alexandre Louvet,", "Nabil Mustafa"]
+++

 Simplicial partitions are a fundamental structure in computational geometry, as they form the basis of optimal data structures for range searching and several related problems. Current algorithms are built on very specific spatial partitioning tools tailored for certain geometric cases. This severely limits their applicability to general set systems. In this work, we propose a simple greedy heuristic for constructing simplicial partitions of any set system. We present a thorough empirical evaluation of its behavior on a variety of geometric and non-geometric set systems, showing that it performs well on most instances. Implementation of these algorithms is available on Github ([C++](https://github.com/alex-louvet/partitions),[Rust](https://github.com/alex-louvet/partitions-rs)).
